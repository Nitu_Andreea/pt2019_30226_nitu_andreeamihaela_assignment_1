package Junit;


import Operatii.*;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import Model.Parsare;
import Model.Polinom;
import Operatii.Impartire;

public class Operatii {
	private Polinom p1,p2;
	public Operatii() {
	        try {
				 p1 = Parsare.toPolinom("x^2-1");
				 p2=Parsare.toPolinom("x+1");
				} catch (Exception e) {
			}
	    }

    @Test public void testAdunare() {
		Polinom p = Adunare.makeResult(p1,p2);
		assertTrue(p.toString().equals("x^2+x"));
	}
	
    @Test public void testDiferenta() {
		Polinom p = Scadere.makeResult(p1,p2);
		System.out.println(p.toString());
		assertTrue(p.toString().equals("x^2-x-2.0"));
	}
	
    @Test public void testInmultire() {
		Polinom p = Imultire.makeResult(p1,p2);
	
		
		assertTrue(p.toString().equals("x^3+x^2-x-1.0"));
	}
	
    @Test public void testImpartire() throws Exception {
		Polinom p[] = Impartire.makeResult(p1,p2);
		assertTrue(p[0].toString().equals("x-1.0") && p[1].toString().equals("0"));
	
	}
	
    @Test public void testDerivare() {
		Polinom p = Derivare.makeResult(p1);
		assertTrue(p.toString().equals("2.0x"));
	}
	
    @Test public void testIntegrare() {
		Polinom p = Integrare.makeResult(p2);
		assertTrue(p.toString().equals("0.5x^2+x"));
	}

}
