package Junit;

import org.junit.Test;

import Model.Parsare;
import Model.Polinom;
import Operatii.Impartire;

public class ExceptionTest {
		static Polinom p1,p2;
		

	 @Test(expected=Exception.class) public void parsare() throws Exception {
		
		 System.out.println(Parsare.toPolinom("xx^2-e1"));
		 
	 }
	 @Test(expected=Exception.class) public void divZero() throws Exception {
			
		 Polinom p1 = Parsare.toPolinom("x^2-1");
		 Polinom p2 = Parsare.toPolinom("0");
		 System.out.println(Impartire.makeResult(p1,p2));
		 
	 }
	 @Test(expected=Exception.class) public void noDiv() throws Exception {
			
		 Polinom p1 = Parsare.toPolinom("x^2-1");
		 Polinom p2 = Parsare.toPolinom("x^10");
		 System.out.println(Impartire.makeResult(p1,p2));
		 
	 }
	   
}
