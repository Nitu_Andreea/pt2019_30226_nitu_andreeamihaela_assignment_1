package Operatii;

import Model.*;

public abstract class Adunare {

	public static Polinom makeResult(Polinom p1, Polinom p2) {
		Polinom poli = new Polinom();
		int i = 0, j = 0;
		int n1 = p1.p.size();									//SALVEZ lungimile pentru a face codul mai citibil
		int n2 = p2.p.size();
		while (i < n1 && j < n2) {								//parcurg polinoamele pana cand unul dintre ele se termina
			int pow1 = p1.p.get(i).getPutere();
			int pow2 = p2.p.get(j).getPutere();
			if (pow1 == pow2) { 								// daca au aceeasi putere, fac adunarea si pun monomul rezultat
				poli.p.add(new Monom(p1.p.get(i).getCoef() + p2.p.get(j).getCoef(), pow1)); // rez=suma lor
				i++;
				j++;
			} else if (pow1 > pow2) {
				poli.p.add(p1.p.get(i)); 						//altfel adauga-l pe cel mai mare, apoi deplaseaza-te
				i++;
			} else {
				poli.p.add(p2.p.get(j));
				j++;
			}
		}
		while (i < n1) {
			poli.p.add(p1.p.get(i)); 							// adauga daca au mai ramas monoame din p1
			i++;
		}
		while (j < n2) {
			poli.p.add(p2.p.get(j)); 							// adauga daca au mai ramas monoame din p2
			j++;
		}
		poli.faCurat();											//sterge eventualele polinoame nule
		return poli;}

}
