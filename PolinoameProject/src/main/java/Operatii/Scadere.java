package Operatii;

import Model.*;

public abstract class Scadere {
	public static Polinom makeResult(Polinom p1, Polinom p2) {
		Polinom poli = new Polinom();
		for (Monom m : p2.p) {
			poli.p.add(new Monom((-1) * m.getCoef(), m.getPutere()));
		}
		Polinom ad = Adunare.makeResult(p1, poli);
		poli = ad;
		return poli;
	}
}
