package Operatii;

import Model.*;

public abstract class Derivare {
	public static Polinom makeResult(Polinom p1) {
		Polinom poli=new Polinom();
		poli = new Polinom();
		for (Monom m : p1.p)																		//SE PARGURGE LISTA SI SE APLICA FORMULA DE DERIVARE
			poli.p.add(new Monom(m.getCoef() * m.getPutere(), m.getPutere() - 1));
		poli.faCurat();
		return poli;
	}
}
