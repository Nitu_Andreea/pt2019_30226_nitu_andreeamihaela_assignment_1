package Operatii;

import Model.Monom;
import Model.Polinom;

public abstract class Imultire {


	public static Polinom makeResult(Polinom p1, Polinom p2) {
			Polinom poli = new Polinom();
			Polinom pol = new Polinom();
			Polinom res = Adunare.makeResult(new Polinom(), new Polinom());
			for (Monom m1 : p1.p) {
				{
					for (Monom m2 : p2.p) {									//SE IMULTESTE UN MONOM DIN p1 CU POLINOMUL p2
						pol.p.add(new Monom(m1.getCoef() * m2.getCoef(), m1.getPutere() + m2.getPutere()));
					}

					res = Adunare.makeResult(res, pol);						//SE FAC ADUNARI SUCCESIVE INTRE POLINOAMELE REZULTATE
					pol.p.clear();											//SE INITIALIZEAZA POLINOMUL P
				}
			}
			poli = res;
		return poli;
	}

}
