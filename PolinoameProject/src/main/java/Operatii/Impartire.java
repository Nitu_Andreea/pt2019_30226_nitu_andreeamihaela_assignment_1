package Operatii;

import Model.*;

public abstract class Impartire {

	public static Polinom[] makeResult(Polinom deimp, Polinom imp) throws Exception {
		Polinom cat = new Polinom();													
		Polinom rest = new Polinom();
		if(imp.p.size()==0) throw new Exception();										//VERIFICAM EXCEPTIA IMPARTIRII LA ZERO
		if (deimp.dominantPow()>=imp.dominantPow()) {									//FACEM IMPARTIREA DOAR DACA GRAD(deimp)>GRAD(imp)
			double coef = deimp.dominantCoef() / imp.dominantCoef();
			int pow = deimp.dominantPow() - imp.dominantPow();							//SE TRATEAZA CAZUL INITIAL, LUCRAND CU deimp
			cat.p.add(new Monom(coef, pow));											//ADAUGAM NOUL MONOM LA CAT
			rest=Imultire.makeResult(new Polinom(new Monom(coef, pow)),imp);			//OBTINEM RESTUL PRIN IMULTIRE SI SCADERE
			rest = Scadere.makeResult(deimp, rest);
		
			Polinom aux = new Polinom();									      //SE TRATEAZA CAZUL GENERAL, LUCRAND CU rest SI CU UN POLINOM auxiliar 
			coef = rest.dominantCoef() / imp.dominantCoef();
			while (rest.dominantPow()>=imp.dominantPow()) {						  //OBTINEM RESTUL DOAR CAND GRAD(rest)<GRAD(imp)
				coef = rest.dominantCoef() / imp.dominantCoef();
				pow = rest.dominantPow() - imp.dominantPow();
				cat.p.add(new Monom(coef, pow));
				aux=Imultire.makeResult(new Polinom(new Monom(coef, pow)),imp);
				rest = Scadere.makeResult(rest, aux);
				aux.p.clear();
			}

			Polinom pvec[] = new Polinom[2];									//SETAM REZULTATUL PENTRU A FI RETURNAT
			pvec[0] = cat;
			pvec[1] = rest;
			return pvec;
		}
		else throw new Exception();
	}

}
