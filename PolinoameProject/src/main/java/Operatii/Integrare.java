package Operatii;

import Model.*;

public abstract class Integrare {
	public static Polinom makeResult(Polinom p1) {	
		Polinom poli = new Polinom();
		for (Monom m : p1.p)								      //SE PARGURGE LISTA SI SE APLICA FORMULA DE INTEGRARE
			poli.p.add(new Monom(m.getCoef() * (1.0 / (m.getPutere()+1)), m.getPutere() + 1));
		poli.faCurat();
		return poli;
	}
}
