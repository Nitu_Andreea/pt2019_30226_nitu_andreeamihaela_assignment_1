package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import Model.*;
import Operatii.*;
import View.*;

public class Controller {
	Model model;
	View view;

	/**CONSTRUCTOR_________________________________________________________________________________________________________________________________*/
	
	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
		listenBtns();
		ordoneaza();
		reseteaza();
	}

	/**CITIRE POLINOAME__________________________________________________________________________________________________________________________________**/
	public void get_info_deriv() {
		try {															
			model.p = Parsare.toPolinom(view.tf.getText());		//SPECIFICA FERESTREI CU DERIVARE..
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Eroare la citire!");
			view.rez2.setText("");
		}
	}

	public void getInf() throws Exception {				//SPECIFICA FERESTREI CU ADUNARE...
		model.p1 = Parsare.toPolinom(view.tf1.getText());
		model.p2 = Parsare.toPolinom(view.tf2.getText());
		if (model.p1 == null || model.p1 == null)
			throw new Exception();
	}

	public void getInfo() {
		try {
			getInf();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Eroare la citire!");
			view.rez.setText("");
		}
	}
	/**BUTOANE DE RESET__________________________________________________________________________________________________________________________________ **/		
	private void reseteaza() {
		view.reset.addActionListener(new ActionListener() {			//RESET DERIVARE

			public void actionPerformed(ActionEvent e) {
				view.tf.setText("");
				view.rez2.setText("");
				model.p.p.clear();
			}

		});
		view.reset1.addActionListener(new ActionListener() {	//RESET P1 ADUNARE

			public void actionPerformed(ActionEvent e) {
				view.tf1.setText("");
				model.p1.p.clear();
			}

		});
		view.reset2.addActionListener(new ActionListener() {   //RESET P2 ADUNARE

			public void actionPerformed(ActionEvent e) {
				view.tf2.setText("");
				model.p2.p.clear();
			}

		});
	}
	/**BUTOANELE ORDONEAZA__________________________________________________________________________________________________________________________________ **/		
	public void ordoneaza() {
		view.ord1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {		//ADUNARE FRAME
				getInfo();
				view.tf2.setText(model.p2.intToString());
				view.tf1.setText(model.p1.intToString());
			}

		});
		view.ord.addActionListener(new ActionListener() {	//DERIVARE FRAME

			public void actionPerformed(ActionEvent arg0) {
				get_info_deriv();
				view.tf.setText(model.p.intToString());
			}

		});
	}
	/**ALTE BUTOANE__________________________________________________________________________________________________________________________________ **/		
	public void listenBtns() {
		view.option1.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				view.jf2.setVisible(true);						//FACE VIZIBILA: DERIVARE FRAME 
			}

		});
		view.option2.addActionListener(new ActionListener() {  //FACE VIZIBILA: ADUNARE FRAME

			public void actionPerformed(ActionEvent e) {
				view.jf1.setVisible(true);
			}

		});
		view.b1.addActionListener(new ActionListener() {		//ADUNARE

			public void actionPerformed(ActionEvent arg0) {
				getInfo();
				Polinom add = Adunare.makeResult(model.p1, model.p2);
				view.rez.setText(add.toString());
			}

		});

		view.b2.addActionListener(new ActionListener() {     //SCADERE

			public void actionPerformed(ActionEvent arg0) {
				getInfo();
				Polinom sub = Scadere.makeResult(model.p1, model.p2);
				view.rez.setText(sub.toString());
			}

		});
		view.b3.addActionListener(new ActionListener() {	  //INMULTIRE

			public void actionPerformed(ActionEvent arg0) {
				getInfo();
				Polinom mul = Imultire.makeResult(model.p1, model.p2);
				view.rez.setText(mul.toString());
			}

		});
		view.b4.addActionListener(new ActionListener() {		//IMPARTIRE

			public void actionPerformed(ActionEvent arg0) {
				getInfo();
				Polinom[] imp = null;
				try {
					imp = Impartire.makeResult(model.p1, model.p2);
					view.rez.setText(imp[0].toString() + " rest:" + imp[1].toString());

				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Nu se poate realiza impartirea");
				}
			}

		});

		view.b5.addActionListener(new ActionListener() {		//DERIVARE

			public void actionPerformed(ActionEvent arg0) {
				get_info_deriv();
				Polinom deriv = Derivare.makeResult(model.p);
				view.rez2.setText(deriv.toString());
			}

		});
		view.b6.addActionListener(new ActionListener() {		//INTEGRARE

			public void actionPerformed(ActionEvent arg0) {
				get_info_deriv();
				Polinom integr = Integrare.makeResult(model.p);
				view.rez2.setText(integr.toString());
			}

		});

	}

}
