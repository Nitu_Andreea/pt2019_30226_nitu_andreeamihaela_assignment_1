package View;

import java.awt.*;
import javax.swing.*;

public class View {
	Color blu=new Color(50, 150, 255);
	Color oranj=new Color(250, 160, 40);
	
	Font myFont = new Font("Serif", Font.BOLD, 30);
	private int width = 1500;
	private int height = 700;
	public JFrame jf, jf1, jf2;
	public JButton option1, option2,b1,b2,b3,b4,b5,b6,ord,ord1,reset1,reset2,reset;
	public JTextField tf1,tf2,tf,rez,rez2;

	/** CONSTRUCTOR__________________________________________________________________________________________________________________________________ **/	
	public View() {
		createMainJf();
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jf1=createJf(getMainPanel1());
		jf2=createJf(getMainPanel2());
		jf1.setVisible(false);
		jf2.setVisible(false);
	}
	/** MAIN PANEL:__________________________________________________________________________________________________________________________________ **/	
	public JPanel getMainPanel() {
		JPanel mainPanel = new JPanel();
		mainPanel.setBackground(blu);
		mainPanel.setLayout(null);

		JLabel titlu1 = new JLabel("Operatii cu polinoame");
		titlu1.setFont(new Font("Serif", Font.BOLD, 40));
		JPanel up = new JPanel();
		up.setBackground(Color.white);
		up.add(titlu1);
		up.setBounds(100, 20, 700, 80);

		JLabel titlu2 = new JLabel("Proiect Realizat de Andreea Nitu");
		titlu2.setBounds(250, 400, 600, 100);
		titlu2.setFont(myFont);
		titlu2.setForeground(Color.WHITE);

		option1 = new JButton("P(x)");
		option1.setFont(myFont);
		option1.setBackground(Color.white);
		option1.setBounds(150, 250, 250, 100);

		option2 = new JButton("P1(x),P2(x)");
		option2.setFont(myFont);
		option2.setBackground(Color.white);
		option2.setBounds(600, 250, 250, 100);

		JLabel o1 = new JLabel("Integrare-Derivare");
		o1.setFont(myFont);
		o1.setBounds(100, 200, 350, 30); // putin mai sus de butonul option1 : 200
		JLabel o2 = new JLabel(" +   -   *   / ");
		o2.setFont(myFont);
		o2.setBounds(600, 170, 350, 100);// coord 2 >250

		mainPanel.add(up);
		mainPanel.add(option1);
		mainPanel.add(option2);
		mainPanel.add(titlu2);
		mainPanel.add(o1);
		mainPanel.add(o2);
		return mainPanel;
	}
	/** DERIVARE/INTEGRARE PANEL:__________________________________________________________________________________________________________________________________ **/	
	public JPanel getMainPanel2() {
		JPanel mainPanel=new JPanel();
		mainPanel.setLayout(null);
		int k=100;
		Font f=new Font("Serif", Font.BOLD, 20);
		
		tf=new JTextField(40);
		tf.setFont(myFont);
		tf.setForeground(Color.white);
		tf.setBackground(Color.BLACK);
		tf.setBounds(100, 60+k, 700,50);
		rez2=new JTextField(40);
		rez2.setBounds(100, 60+2*k, 700,50);
		rez2.setBackground(blu);
		rez2.setFont(myFont);
		rez2.setForeground(Color.black);
	
		JLabel l2=new JLabel("P(x):");
		l2.setBounds(20, 50+k, 700,50);
		l2.setFont(f);
		reset=newBtn("reset",20);
		reset.setBackground(Color.WHITE);
		reset.setBounds(5,90+k,80,20);
		JLabel l3=new JLabel("Rez(x):");
		l3.setBounds(20, 50+2*k, 700,50);
		l3.setFont(f);
		
		JPanel btns=new JPanel();
		btns.setBounds(90,60+3*k,700,100);
		b5=newBtn("Derivare",30);
		b6=newBtn("Integrare",30);
		ord=newBtn("Ordonare P(x)",30);
		ord.setBackground(Color.white);
		
		btns.add(b5);
		btns.add(b6);
		btns.add(ord);
		
		mainPanel.add(tf);
		mainPanel.add(l2);
		mainPanel.add(l3);
		mainPanel.add(rez2);
		mainPanel.add(btns);
		mainPanel.add(reset);
		return mainPanel;
	}
	/** ADUNARE,SCADERE,IMULTIRE,IMPARTIRE PANEL:__________________________________________________________________________________________________________________________________ **/	
	public JPanel getMainPanel1() {
		JPanel mainPanel=new JPanel();
		mainPanel.setLayout(null);
		int k=100;
		Font f=new Font("Serif", Font.BOLD, 20);
		 reset1=newBtn("reset",20);
		 reset2=newBtn("reset",20);
		 
		tf1=new JTextField(40);
		tf1.setFont(myFont);
		tf1.setForeground(Color.white);
		tf1.setBackground(Color.BLACK);
		tf1.setBounds(100, 60, 700,50);
		
		tf2=new JTextField(40);
		tf2.setFont(myFont);
		tf2.setForeground(Color.white);
		tf2.setBackground(Color.BLACK);
		tf2.setBounds(100, 60+k, 700,50);
		
		rez=new JTextField(40);
		rez.setBounds(100, 60+2*k, 700,50);
		rez.setBackground(blu);
		rez.setFont(myFont);
		rez.setForeground(Color.black);
	
		JLabel l1=new JLabel("P1(x):");
		l1.setBounds(20, 50, 700,50);
		reset1.setBounds(5, 90, 80,20);
		reset1.setBackground(Color.white);
		l1.setFont(f);
		JLabel l2=new JLabel("P2(x):");
		l2.setBounds(20, 50+k, 700,50);
		reset2.setBounds(5,90+k, 80,20);
		reset2.setBackground(Color.white);

		l2.setFont(f);
		JLabel l3=new JLabel("Rez(x):");
		l3.setBounds(20, 50+2*k, 700,50);
		l3.setFont(f);
		
		JPanel btns=new JPanel();
		btns.setBounds(90,60+3*k,700,100);
		 b1=newBtn("Adunare",30);
		 b2=newBtn("Scadere",30);
		 b3=newBtn("Imultire",30);
		 b4=newBtn("Impartire",30);
		 ord1=newBtn("Ordonare P1,P2",30);
		 ord1.setBackground(Color.white);
		 
		btns.add(b1);
		btns.add(b2);
		btns.add(b3);
		btns.add(b4);
		btns.add(ord1);
		
		mainPanel.add(l1);
		mainPanel.add(tf1);
		mainPanel.add(l2);
		mainPanel.add(tf2);
		mainPanel.add(l3);
		mainPanel.add(rez);
		mainPanel.add(btns);

		mainPanel.add(reset1);
		mainPanel.add(reset2);
		return mainPanel;
	}
	/** MODEL DE CREARE BUTON:__________________________________________________________________________________________________________________________________ **/	
	private JButton newBtn(String string,int k) {
		JButton b=new JButton(string);
		b.setFont(new Font("Serif", Font.BOLD, k));
		b.setBackground(oranj);
		return b;
	}
	/** OPERATIE FRAME:__________________________________________________________________________________________________________________________________ **/	
	public JFrame createJf(JPanel mainPanel) {
		JFrame jf = new JFrame();

		mainPanel.setBounds(220, 50, 900, 500);

		ImageIcon i = new ImageIcon("c.png");
		JLabel photo = new JLabel();
		photo.setIcon(i);
		photo.setBounds(0, 0, 2000, 2000);
		photo.add(mainPanel);
		jf.setSize(width, height);
		jf.setContentPane(photo);
		finishFrame(jf);
		return jf;
	}
	/** MAIN FRAME:__________________________________________________________________________________________________________________________________ **/	
	public void createMainJf() {
		jf = new JFrame();

		JPanel mainPanel = getMainPanel();
		mainPanel.setBounds(220, 50, 900, 500);

		ImageIcon i = new ImageIcon("b.png");
		JLabel photo = new JLabel();
		photo.setIcon(i);
		photo.setBounds(0, 0, 2000, 2000);
		photo.add(mainPanel);
		jf.setSize(width, height);
		jf.setContentPane(photo);
		finishFrame(jf);
	}
	/** TERMINA FRAME-UL:__________________________________________________________________________________________________________________________________ **/	
	private void finishFrame(JFrame jf) {
		jf.setVisible(true);
		jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	/** GET/SET:__________________________________________________________________________________________________________________________________ **/	
	public int getWidth() {
		return width;
	}

	public void setWidth(int dWidth) {
		this.width = dWidth;
	}

	public int getHeight() {
		return height;
	}

	public void setdHeight(int dHeight) {
		this.height = dHeight;
	}

}
