package Model;

public class Monom {
	private double coef;
	private int putere;

/** CONSTRUCTOR_____________________________________________________________________________________________________________________________________ **/
	public Monom(double coef, int putere) {
		this.coef = coef;
		this.putere = putere;
	}

/**TO STRING: INT si DOUBLE____________________________________________________________________________________________________________________________**/
	/** Tostring: start:nu pune + la inceput
	 *                  al doilea pune mereu semn de legatura**/
	public String startToString() {
		if(putere==0 &&Math.abs(coef)==1) return coef+"";//caz special: punem 1 doar cand puterea e 0
		String s = "";
		if(coef==-1) s+="-";	//nu afisam
		else if(coef!=1) s+=coef;    // coeficientul 1
		if(putere>=1) {//nu afisam puterea 1 si 0
			s += "x";
			if (putere>1) 
				s += "^" + putere;
		}
		return s;
	}
	public String toString() {
		String s = "";
		if(coef>=0) s+="+";//s[0]o sa fie mereu un semn. plusul trebuie atasat
		if(putere==0 &&Math.abs(coef)==1) return s+coef;//caz special: punem 1 doar cand puterea e 0
		if(coef==-1) s+="-";	//nu afisam coeficientul 1
			else if(coef!=1) s+=coef;    
		if(putere>=1) {//nu afisam puterea 1 si 0
			s += "x";
			if (putere>1)	
			s += "^" + putere;
		}
		return s;
	}
	public String intStartToString() {
		if(putere==0 &&Math.abs(coef)==1) return (int)coef+"";//caz special: punem 1 doar cand puterea e 0
		String s = "";
		if(coef==-1) s+="-";	//nu afisam
			else if(coef!=1) s+=(int)coef;    // coeficientul 1
		if(putere>=1) {//nu afisam puterea 1 si 0
			s += "x";
			if (putere>1)	
			s += "^" + putere;
		}
		return s;
	}
	public String intToString() {
		String s = "";
		if(coef>=0) s+="+";//s[0]o sa fie mereu un semn. plusul trebuie atasat
		if(putere==0 &&Math.abs(coef)==1) return s+(int)coef;//caz special: punem 1 doar cand puterea e 0
		if(coef==-1) s+="-";	//nu afisam coeficientul 1
			else if(coef!=1) s+=(int)coef;    		
		if(putere>=1) {//nu afisam puterea 1 si 0
			s += "x";
			if (putere>1)	
			s += "^" + putere;
		}
		return s;
	}
/** pentru grupare:__________________________________________________________________________________________________________________________________ **/	
	public void add(Monom m) {
		if (m.putere == this.putere)
			coef += m.getCoef();
	}

/** GET/SET__________________________________________________________________________________________________________________________________________ **/
	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}

	public int getPutere() {
		return putere;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}

	
}
