package Model;

public abstract class Parsare {
	static int posPow, posX = 0;
	static int i = 0;
	/**VERIFICA EXCEPTIILE IN TIMP CE PARCURGEM SIRUL__________________________________________________________________________________________________**/
	public static String checkIf(String s) {
		if (s.charAt(1) == 'x') {						//PENTRU A SE REALIZA PARSAREA FACEM TRANSFORMAREA [(sign)x->(sign)1x], unde (sign)={-,+}
			s = s.charAt(0) + "1" + s.substring(1);
			i++;										//ne pozitionam cu contorul peste numarul adaugat
			if (posPow != 0)							//actualizam pozitia puterii daca exista
				posPow++;
			posX++;										//actualizam pozitia lui x
		} 
     return s;
     }
/** PARSAREA __________________________________________________________________________________________________**/
	public static Polinom toPolinom(String s) throws Exception {
		Polinom p = new Polinom();
		Monom m = new Monom(0, 0);
		s = startCheck(s);
		while ((i < s.length())) {
			if (s.charAt(i) == '-' || s.charAt(i) == '+') {
				s = checkIf(s);
				if (posPow != 0) { // if(posPow<=2) m.setCoef(1);
					m.setCoef(Double.parseDouble(s.substring(0, posPow - 1)));
					m.setPutere(Integer.parseInt(s.substring(posPow + 1, i)));
				} else if (posX != 0) {
					m.setCoef(Double.parseDouble(s.substring(0, posX)));
					m.setPutere(1);
				} else {
					m.setCoef(Double.parseDouble(s.substring(0, i)));
				}
				p.p.add(new Monom(m.getCoef(), m.getPutere())); // punem monomul in polinom
				m.setPutere(0); // initializari
				s=reInit(s);
				
			} else {
				switch (s.charAt(i)) {
				case '^':
					posPow = i;
					break;
				case 'x':
					posX = i;
					break;
				default:
					if (!(s.charAt(i) >= '0' && s.charAt(i) <= '9'))
						throw new Exception();
				}
			}
			i++;
		}
		p = finish_pol(p);
		return p;
	}
/** INITIALIZARI IN DECURSUL PARSARII__________________________________________________________________________________________________**/
	private static String reInit(String s) {
		s = s.substring(i, s.length()); 					// MONOMUL EXTRAS SE STERGE DIN STRINGUL DE PARSAT
		posPow = 0;
		posX = 0;
		i = 0;
		return s;
	}
	/** __________________________________________________________________________________________________**/
	private static String startCheck(String s) {
		i=0;						//FIXAM POZITIILE INITIALE
		posX=0;
		posPow=0;
		s = s + '+'; 				//PENTRU A ASIGURA TRATAREA ULTIMULUI POLINOM
		if (s.charAt(0) == '-'||s.charAt(0)=='+') {
			i++;
		} else {
			s = "+" + s;			//PENTRU A ASIGURA PARSAREA CORECTA
			i++;
		}
		return s;
	}
	/** ORGANIZAM POLINOMUL PENTRU A FI RETURNAT__________________________________________________________________________________________________**/
	public static Polinom finish_pol(Polinom p) {
		p.grupare();
		p.faCurat();
		p.sortare();
		return p;
	}
}
