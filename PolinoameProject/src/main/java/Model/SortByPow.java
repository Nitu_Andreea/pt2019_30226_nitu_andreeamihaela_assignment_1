package Model;

import java.util.Comparator;

public class SortByPow implements Comparator<Monom> {

	public int compare(Monom m1, Monom m2) {
		return m2.getPutere() - m1.getPutere();
	}

}
