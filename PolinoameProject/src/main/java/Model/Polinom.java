package Model;

import java.util.ArrayList;
import java.util.Iterator;

public class Polinom {
	public ArrayList<Monom> p;
/** CONSTRUCTORI_______________________________________________________________________________________________________________________________ **/
	public Polinom() {
		p = new ArrayList<Monom>();
	}
	public Polinom(Monom monom) {
		p = new ArrayList<Monom>();
		p.add(monom);
		
	}
/** TO STRING__________________________________________________________________________________________________________________________________ **/
	public String toString() {				//pentru numere reale
		if (p.size() != 0) {
			String s = p.get(0).startToString();  //scriem initial primul monom
			Iterator<Monom> i = p.iterator();     //apoi parcurgem lista 
			i.next();							  //incepand de pe a doua pozitie
			while (i.hasNext())
				s += i.next().toString();
			return s;
		}
		return "0";
	}
	public String intToString() {
		if (p.size() != 0) {		     //pentru numere intregi
			String s = p.get(0).intStartToString();
			Iterator<Monom> i = p.iterator();
			i.next();
			while (i.hasNext())
				s += i.next().intToString();
			return s;
		}
		return "";
	}
	/** SORTARE_____________________________________________________________________________________________________________________________________**/
	public void sortare() {
		p.sort(new SortByPow());
	}
	/**Dominant pt impartire________________________________________________________________________________________________________________________**/
	public int dominantPow() {
		if(p.size()>0)return p.get(0).getPutere();
		else return -1;
	}
	public double dominantCoef() {
		if (p.size()>0) return p.get(0).getCoef();
		return -1;
	}
	/**GRUPARE _____________________________________________________________________________________________________________________________________**/
	public void grupare() {
		this.sortare();
		int i = 0;
		while (i <= p.size() - 2) {
			if (p.get(i).getPutere() != p.get(i + 1).getPutere()) {
				i++;
			} else {
				p.get(i + 1).add(p.get(i));
				p.remove(p.get(i));
			}
		}
	}
	/** STERGE POLINOAMELE NULE__________________________________________________________________________________________________________________**/
	public void faCurat() {
		Iterator<Monom> i = p.iterator();
		while (i.hasNext()) {
			if (i.next().getCoef() == 0.000) {
				i.remove();
			}
		}
	}

}
